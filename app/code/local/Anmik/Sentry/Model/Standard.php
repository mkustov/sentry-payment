<?php

class Anmik_Sentry_Model_Standard extends Mage_Payment_Model_Method_Cc
{
    protected $_code = 'sentry';
    protected $_isGateway = true;
    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_canCapturePartial = false;
    protected $_canRefund = false;
    protected $_canVoid = false;
    protected $_canUseInternal = true;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = true;
    protected $_canSaveCc = false;
    protected $_authorizeParams = array(
        'merid'                 => '',
        'acqid'                 => '',
        'sentry_server_url'     => '',
        'signature_password'    => '',
    );
    protected $_hasEmptyParams = false;

    const PROTOCOL_VERSION = '1.0.0';
    const PURCHASE_CURRENCY = '643';
    const PURCHASE_CURRENCY_EXPONENT = '2';
    const SIGNATURE_METHOD = 'SHA1';
    const REQUIRED_AMOUNT_LENGTH = 12;

    protected function _initData(){
        foreach($this->_authorizeParams as $key => $value){
            $value = Mage::getStoreConfig('payment/sentry/'.$key);
            if($value  !== '' && $value !== null){
                $this->_authorizeParams[$key] = $value;
            } else{
                $this->_hasEmptyParams = true;
            }
        }
    }

    protected function _prepareAmount($amount){
        $amount = preg_replace('/[^0-9]/', '', $amount);
        if(strlen($amount) < self::REQUIRED_AMOUNT_LENGTH){
            $numberOfZero = self::REQUIRED_AMOUNT_LENGTH - strlen($amount);
            for($i = 0; $i < $numberOfZero; $i++){
                $amount = '0'.$amount;
            }
        }
        return $amount;
    }

    protected function _prepareData(Varien_Object $payment, $amount){
        $data = array();
        $data['Version'] = self::PROTOCOL_VERSION;
        $data['MerID'] = $this->_authorizeParams['merid'];
        $data['AcqID'] = $this->_authorizeParams['acqid'];
        $data['MerRespURL'] = Mage::getBaseUrl();
        $data['PurchaseCurrency'] = self::PURCHASE_CURRENCY;
        $data['PurchaseCurrencyExponent'] = self::PURCHASE_CURRENCY_EXPONENT;
        $data['OrderID'] = $payment->getOrder()->getIncrementId();
        $data['SignatureMethod'] = self::SIGNATURE_METHOD;
        $data['PurchaseAmt'] = $this->_prepareAmount($amount);
        $data['Signature'] = base64_encode(hex2bin(sha1($this->_authorizeParams['signature_password'].$this->_authorizeParams['merid'].$this->_authorizeParams['acqid'].$data['OrderID'].$data['PurchaseAmt'].$data['PurchaseCurrency'])));
        return $data;
    }

    protected function _makeRequest($data){
        $post = '';
        foreach ($data as $key => $value) {
            if ($value != '') {
                $post .= '&' . $key . '=' . $value;
            }
        }
        $req = curl_init();
        curl_setopt($req, CURLOPT_POST, 1);
        curl_setopt($req, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($req, CURLOPT_URL, $this->_authorizeParams['sentry_server_url']);
        curl_setopt($req, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($req);
        curl_close($req);
        return $result;
    }

    /**
     * Capture payment
     *
     * @param Varien_Object $payment
     * @param float $amount
     *
     * @return Mage_Payment_Model_Abstract
     */
    public function capture(Varien_Object $payment, $amount)
    {
        if (!$this->canCapture()) {
            Mage::throwException(Mage::helper('payment')->__('Capture action is not available.'));
        }

        $this->_initData();

        if($this->_hasEmptyParams){
            Mage::throwException(Mage::helper('payment')->__('Need to fill all params in admin panel for SENTRY payment.'));
        }

        $data = $this->_prepareData($payment, $amount);
        $response = $this->_makeRequest($data);

        return $this;
    }
}